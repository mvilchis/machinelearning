#! /usr/bin/python

from sklearn import svm
from sklearn import datasets
clf = svm.SVC()
iris = datasets.load_iris()
clf.fit(iris.data, iris.target_names[iris.target])

from sklearn.externals import joblib
joblib.dump(clf, 'predictor.pkl')
