#!/usr/bin/python

"""
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:
    Sara has label 0
    Chris has label 1
"""

import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###

#########################################################

from sklearn import svm
import time
clf = svm.SVC(kernel = "linear")
t1 = time.time()
clf.fit(features_train, labels_train)
print time.time() - t1
lables_predict = clf.predict(features_test)
from sklearn.metrics import accuracy_score
print accuracy_score(lables_predict, labels_test)

from sklearn.naive_bayes import GaussianNB
clf = GaussianNB()
t2 = time.time()
clf.fit(features_train, labels_train)
print time.time() - t2
