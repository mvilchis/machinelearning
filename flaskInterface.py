#!flask/bin/python
from flask import Flask, jsonify

app = Flask(__name__)
from flask import abort
from flask import request
clf = 0
@app.route('/todo/api/v1.0/predict', methods=['POST'])
def predict_item():
    global clf
    if not request.json or not 'attributes' in request.json:
        abort(400)
    all_items = []
    for attribute in request.json['attributes'].split("["):
        if attribute:
            features_string = attribute.replace("[", "").replace("]","").split(",")
            features = [ item for item in features_string if item.strip()]
            all_items.append(features)
    prediction =  clf.predict(all_items)
    return jsonify({'prediction':"%s"%(prediction) })

if __name__ == '__main__':
    from sklearn.externals import joblib
    clf = joblib.load('predictor.pkl')
    app.run(debug=True)
